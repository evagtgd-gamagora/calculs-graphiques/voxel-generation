﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImplicitGrid {

    public Vector3 origin;

    public GameObject voxelPrefab;
    private float voxelSize;

    public int nX, nY, nZ;

    public Dictionary<Vector3Int, double> grid;
    public List<GameObject> voxels;

    public List<Sphere> spheres;


    public ImplicitGrid(Vector3 origin, GameObject voxelPrefab)
    {
        this.voxelPrefab = voxelPrefab;
        grid = new Dictionary<Vector3Int, double>();
        voxels = new List<GameObject>();
        
        voxelSize = voxelPrefab.transform.localScale.x;

        nX = 0;
        nY = 0;
        nZ = 0;

        spheres = new List<Sphere>();
    }

    public void AddSphere(Sphere sphere)
    {
        spheres.Add(sphere);

        nX = Mathf.Max(nX, 2 * Mathf.CeilToInt((sphere.radius + Mathf.Abs(sphere.center.x - origin.x)) / voxelSize));
        nY = Mathf.Max(nY, 2 * Mathf.CeilToInt((sphere.radius + Mathf.Abs(sphere.center.y - origin.y)) / voxelSize));
        nZ = Mathf.Max(nZ, 2 * Mathf.CeilToInt((sphere.radius + Mathf.Abs(sphere.center.z - origin.z)) / voxelSize));

        CreateVolume();
    }

    public void ClearSpheres()
    {
        spheres.Clear();
    }

    public void CreateVolume()
    {
        grid.Clear();

        Vector3Int voxelIndex = Vector3Int.zero;
        Vector3Int incrementZ = new Vector3Int(0, 0, 1);

        for (int x = -nX; x <= nX; ++x)
        {
            for (int y = -nY; y <= nY; ++y)
            {
                for (int z = -nZ; z <= nZ; ++z)
                {
                    voxelIndex = new Vector3Int(x, y, z);
                    Vector3 voxelPosition = GetVoxelPosition(voxelIndex);
                    double capacity = TestImplicitVoxelSpheres(voxelPosition);
                    grid.Add(voxelIndex, capacity);

                    voxelIndex += incrementZ;
                }
                voxelIndex += Vector3Int.up;
            }
            voxelIndex += Vector3Int.right;
        }
    }

    public double TestImplicitVoxelSpheres(Vector3 voxelPosition)
    {
        double capacity = 0.0f;

        foreach (Sphere sphere in spheres)
        {
            capacity += TestVoxelSphere(voxelPosition, sphere);
        }

        return capacity;
    }

    public double TestVoxelSphere(Vector3 voxelPosition, Sphere sphere)
    {
        float x = (voxelPosition - sphere.center).magnitude / sphere.radius;

        if (x < 0 || x > 2)
            return 0;

        double result = Mathf.Exp(-Mathf.Pow(x, 2));

        return result;
    }

    public Vector3 GetVoxelPosition(Vector3Int voxelIndex)
    {
        Vector3 voxelPosition = voxelIndex;
        voxelPosition = voxelPosition * voxelSize - origin;
        return voxelPosition;
    }
}


