﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImplicitGridManager : MonoBehaviour {

    public GameObject voxelPrefab;
    ImplicitGrid implicitGrid;

    public Remove remove;

    public int select = 0;


    [Range(0.1f, 1)]
    public double trigger;

    // Use this for initialization
    void Awake () {
        implicitGrid = new ImplicitGrid(Vector3.zero, voxelPrefab);
    }

    private void Update()
    {
        if(Input.GetKey(KeyCode.KeypadPlus))
        {
            switch(select)
            {
                case 0 :
                    StartCoroutine("InstantiationSequence_0");
                    break;
                case 1:
                    StartCoroutine("InstantiationSequence_1");
                    break;
                case 2:
                    StartCoroutine("InstantiationSequence_2");
                    break;
                case 3:
                    StartCoroutine("InstantiationSequence_3");
                    break;

                
            }

            ++select;
            if (select > 3)
                select = 0;
        } 
        else if (Input.GetKey(KeyCode.KeypadEnter))
        {
            InstantiateGridElements(implicitGrid);
        }
    }


    public void InstantiateGridElements(ImplicitGrid implicitGrid)
    {
        ClearGridElements(implicitGrid);

        Sphere removeSphere = new Sphere();
        removeSphere.center = remove.transform.position;
        removeSphere.radius = remove.radius;

        foreach(KeyValuePair<Vector3Int, double> gridEntry in implicitGrid.grid)
        { 
            if (gridEntry.Value - implicitGrid.TestVoxelSphere(gridEntry.Key, removeSphere)  > trigger)
            {
                Vector3 voxelPosition = implicitGrid.GetVoxelPosition(gridEntry.Key);

                GameObject vox = Instantiate(voxelPrefab, voxelPosition, Quaternion.identity); 
                implicitGrid.voxels.Add(vox);
            }
        }
    }

    public void ClearGridElements(ImplicitGrid implicitGrid)
    {
        foreach(GameObject vox in implicitGrid.voxels)
        {
            Destroy(vox);
        }

        implicitGrid.voxels.Clear();
    }

    private void InstantiationSequence_0()
    {
        implicitGrid.ClearSpheres();
        InstantiateGridElements(implicitGrid);
    }

    private void InstantiationSequence_1()
    {
        Sphere sphere_1 = new Sphere();
        sphere_1.radius = 5;
        sphere_1.center = Vector3.up * 6;

        implicitGrid.AddSphere(sphere_1);
        InstantiateGridElements(implicitGrid);
    }

    private void InstantiationSequence_2()
    {
        Sphere sphere_2 = new Sphere();
        sphere_2.radius = 5;
        sphere_2.center = Vector3.down * 6;

        implicitGrid.AddSphere(sphere_2);
        InstantiateGridElements(implicitGrid);
    }

    private void InstantiationSequence_3()
    {
        Sphere sphere_3 = new Sphere();
        sphere_3.radius = 5;
        sphere_3.center = Vector3.right * 6;

        implicitGrid.AddSphere(sphere_3);
        InstantiateGridElements(implicitGrid);
    }

    public ImplicitGrid GetImplicitGrid()
    {
        return implicitGrid;
    }
}
