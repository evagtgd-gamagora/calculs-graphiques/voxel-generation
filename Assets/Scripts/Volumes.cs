﻿using UnityEngine;
public struct Sphere
{
    public Vector3 center;
    public float radius;
}

public enum Mode
{
    Instersection,
    Union
}