﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumicGridManager : MonoBehaviour {

    public GameObject voxelPrefab;
    VolumicGrid volumicGrid;

    public int select = 0;

    // Use this for initialization
    void Awake () {
        volumicGrid = new VolumicGrid(Vector3.zero, voxelPrefab, Mode.Union);
    }

    private void Update()
    {
        if(Input.GetKey(KeyCode.KeypadEnter))
        {
            switch(select)
            {
                case 0 :
                    StartCoroutine("InstantiationSequence_0");
                    break;
                case 1:
                    StartCoroutine("InstantiationSequence_1");
                    break;
                case 2:
                    StartCoroutine("InstantiationSequence_2");
                    break;
                case 3:
                    StartCoroutine("InstantiationSequence_3");
                    break;
                case 4:
                    StartCoroutine("InstantiationSequence_4");
                    break;
            }

            ++select;
            if (select > 4)
                select = 0;
        }
    }


    public void InstantiateGridElements(VolumicGrid volumicGrid)
    {
        ClearGridElements(volumicGrid);

        foreach(KeyValuePair<Vector3Int, bool> gridEntry in volumicGrid.grid)
        { 
            if (gridEntry.Value)
            {
                Vector3 voxelPosition = volumicGrid.GetVoxelPosition(gridEntry.Key);

                GameObject vox = Instantiate(voxelPrefab, voxelPosition, Quaternion.identity); 
                volumicGrid.voxels.Add(vox);
            }
        }
    }

    public void ClearGridElements(VolumicGrid volumicGrid)
    {
        foreach(GameObject vox in volumicGrid.voxels)
        {
            Destroy(vox);
        }

        volumicGrid.voxels.Clear();
    }

    private void InstantiationSequence_0()
    {
        volumicGrid.ClearSpheres();
        volumicGrid.SetMode(Mode.Union);
        InstantiateGridElements(volumicGrid);
    }

    private void InstantiationSequence_1()
    {
        Sphere sphere_1 = new Sphere();
        sphere_1.radius = 10;
        sphere_1.center = Vector3.up * 7;

        volumicGrid.AddSphere(sphere_1);
        InstantiateGridElements(volumicGrid);
    }

    private void InstantiationSequence_2()
    {
        Sphere sphere_2 = new Sphere();
        sphere_2.radius = 10;
        sphere_2.center = Vector3.down * 7;

        volumicGrid.AddSphere(sphere_2);
        InstantiateGridElements(volumicGrid);
    }

    private void InstantiationSequence_3()
    {
        Sphere sphere_3 = new Sphere();
        sphere_3.radius = 10;
        sphere_3.center = Vector3.right * 7;

        volumicGrid.AddSphere(sphere_3);
        InstantiateGridElements(volumicGrid);
    }

    private void InstantiationSequence_4()
    {
        volumicGrid.SetMode(Mode.Instersection);
        InstantiateGridElements(volumicGrid);
    }

    public VolumicGrid GetVolumicGrid()
    {
        return volumicGrid;
    }
}
