﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumicGrid {

    public Vector3 origin;

    public GameObject voxelPrefab;
    private float voxelSize;

    public int nX, nY, nZ;

    public Dictionary<Vector3Int, bool> grid;
    public List<GameObject> voxels;

    public List<Sphere> spheres;

    private Mode mode;


    public VolumicGrid(Vector3 origin, GameObject voxelPrefab, Mode mode)
    {
        this.voxelPrefab = voxelPrefab;
        grid = new Dictionary<Vector3Int, bool>();
        voxels = new List<GameObject>();
        
        voxelSize = voxelPrefab.transform.localScale.x;

        nX = 0;
        nY = 0;
        nZ = 0;

        spheres = new List<Sphere>();
        this.mode = mode;
    }

    public void AddSphere(Sphere sphere)
    {
        spheres.Add(sphere);

        nX = Mathf.Max(nX, Mathf.CeilToInt((sphere.radius + Mathf.Abs(sphere.center.x - origin.x)) / voxelSize));
        nY = Mathf.Max(nY, Mathf.CeilToInt((sphere.radius + Mathf.Abs(sphere.center.y - origin.y)) / voxelSize));
        nZ = Mathf.Max(nZ, Mathf.CeilToInt((sphere.radius + Mathf.Abs(sphere.center.z - origin.z)) / voxelSize));

        CreateVolume();
    }

    public void ClearSpheres()
    {
        spheres.Clear();
    }

    public void CreateVolume()
    {
        grid.Clear();

        Vector3Int voxelIndex = Vector3Int.zero;
        Vector3Int incrementZ = new Vector3Int(0, 0, 1);

        for (int x = -nX; x <= nX; ++x)
        {
            for (int y = -nY; y <= nY; ++y)
            {
                for (int z = -nZ; z <= nZ; ++z)
                {
                    voxelIndex = new Vector3Int(x, y, z);
                    Vector3 voxelPosition = GetVoxelPosition(voxelIndex);
                    bool result = TestVoxelSpheresMode(voxelPosition);
                    grid.Add(voxelIndex, result);

                    voxelIndex += incrementZ;
                }
                voxelIndex += Vector3Int.up;
            }
            voxelIndex += Vector3Int.right;
        }
    }

    private bool TestVoxelSpheresMode(Vector3 voxelPosition)
    {
        bool test = false;

        switch(mode)
        {
            case Mode.Instersection :
                test = true;
                foreach(Sphere sphere in spheres)
                {
                    test &= TestVoxelSphere(voxelPosition, sphere);
                }
                break;

            case Mode.Union:
                test = false;
                foreach (Sphere sphere in spheres)
                {
                    test |= TestVoxelSphere(voxelPosition, sphere);
                }
                break;
        }

        return test;
    }

    private bool TestVoxelSphere(Vector3 voxelPosition, Sphere sphere)
    {
        float f = Mathf.Pow(voxelPosition.x - sphere.center.x, 2)
            + Mathf.Pow(voxelPosition.y - sphere.center.y, 2)
            + Mathf.Pow(voxelPosition.z - sphere.center.z, 2)
            - Mathf.Pow(sphere.radius, 2);
        return f < 0;
    }

    public Vector3 GetVoxelPosition(Vector3Int voxelIndex)
    {
        Vector3 voxelPosition = voxelIndex;
        voxelPosition = voxelPosition * voxelSize - origin;
        return voxelPosition;
    }

    public void SetMode(Mode mode)
    {
        this.mode = mode;
        CreateVolume();
    }
}


